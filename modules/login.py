import os
import getpass
from keystoneauth1 import loading
from keystoneauth1 import session
from openstack import connection
from openstack import exceptions
from rich.console import Console

console = Console()

def login():
    console.print("[bold yellow]OpenStack Login[/bold yellow]")

    try:
        # Solicitar al usuario que ingrese las credenciales
        username = input("Ingrese el nombre de usuario: ")
        password = getpass.getpass("Ingrese la contraseña: ")
        project_name = input("Ingrese el nombre del proyecto: ")

        # Cargar las credenciales en un objeto auth
        loader = loading.get_plugin_loader('password')
        auth = loader.load_from_options(
            auth_url="http://ospcloudmh-controller-1.local.lan:5000/v3",
            username="admin",
            password="openstack",
            project_name="admin",
            user_domain_name="Default",
            project_domain_name="Default"
        )

        # Crear una sesión
        sess = session.Session(auth=auth)

        # Crear una conexión utilizando la biblioteca openstacksdk
        conn = connection.Connection(session=sess)

        try:
            # Consultar un token para verificar la autenticación
            token = conn.authorize()

            if token:
                console.print("[bold green]Inicio de sesión exitoso![/bold green]")
                return conn
            else:
                console.print("[bold red]Error de autenticación. Las credenciales son incorrectas.[/bold red]")
                return None
        except exceptions.HttpException as e:
            console.print("[bold red]Error de conexión: {}[/bold red]".format(e))
            return None
        except exceptions.SDKException as e:
            console.print("[bold red]Error de SDK: {}[/bold red]".format(e))
            return None
    except Exception as e:
        console.print("[bold red]Error: {}[/bold red]".format(e))
        return None

def get_token(connection):
    # Obtener el token
    console.print("Token:", connection.session.get_token())

def main_menu():
    console = Console()

    panel_text = "\n".join(['[bold red]Bienvenido al OpenStack Manager[/bold red]', '1.Iniciar Sesion', '2. Salir'])
    panel = Panel(panel_text, title='Menu Principal')

    console.print(panel)

def main():
    while True:
        main_menu()
        choice = input("Seleccione una opción: ")

        if choice == "1":
            connection = login()
            if connection is not None:
                openstack_operations(connection)
                get_token(connection)

        elif choice == "2":
            print("Hasta luego.")
            break

if __name__ == "__main__":
    main()
