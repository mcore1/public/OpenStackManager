from menu_utils import generate_menu

def openstack_menu():
    choices = [
        ('1', 'Keystone'),
        ('2', 'Glance'),
        ('3', 'Nova'),
        ('4', 'Neutron'),
        ('5', 'Cinder'),
        ('6', 'Atrás'),
        ('7', 'Salir')
    ]
    
    choice = generate_menu(choices, "Menú de OpenStack")
    
    return choice


def openstack_operations(connection):
    while True:
        choice = openstack_menu()

        if choice == '1. Keystone':
            # Lógica para la opción Keystone
            from menu.keystone_menu import keystone_operations
            keystone_operations(connection)

        elif choice == '2. Glance':
            # Lógica para la opción Glance
            pass

        elif choice == '3. Nova':
            # Lógica para la opción Nova
            pass

        elif choice == '4. Neutron':
            # Lógica para la opción Neutron
            pass

        elif choice == '5. Cinder':
            # Lógica para la opción Cinder
            pass

        elif choice == '6. Atrás':
            # Opción para ir atrás
            return

        elif choice == '7. Salir':
            # Opción para salir
            print("Hasta luego.")
            exit()

        else:
            print("Opción inválida. Por favor, seleccione una opción válida.")
