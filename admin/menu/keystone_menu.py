from operations.keystone_operations import get_token, create_user, delete_user, list_users
from menu_utils import generate_menu

def keystone_operations(connection):
    while True:
        choices = [
            ('1', 'Obtener token'),
            ('2', 'Listar usuarios'),
            ('3', 'Crear usuario'),
            ('4', 'Eliminar usuario'),
            ('5', 'Atrás')
        ]
        
        choice = generate_menu(choices, "Menú de Keystone")
        choice_number = choice.split('.')[0].strip()  # Obtener el número de opción
        
        if choice_number == '1':
            get_token(connection)
        elif choice_number == '2':
            list_users(connection)
        elif choice_number == '3':
            create_user(connection)
        elif choice_number == '4':
            list_users(connection)
            username = input("Ingrese el nombre de usuario a eliminar: ")
            delete_user(connection, username)
        elif choice_number == '5':
            return
        else:
            print("Opción inválida. Por favor, seleccione una opción válida.")
