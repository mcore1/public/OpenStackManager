import inquirer

def generate_menu(choices, title):
    questions = [
        inquirer.List('choice',
                      message=f"Seleccione una opción ({title}):",
                      choices=[f"{num}. {name}" for num, name in choices]),
    ]
    answers = inquirer.prompt(questions)
    return answers['choice']