import sys
import os
from getpass import getpass
from menu.openstack_menu import openstack_operations
from rich.console import Console
from rich.panel import Panel
from menu_utils import generate_menu


# Agregar la ruta del directorio raíz del proyecto al sys.path
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(root_dir)

from modules.login import login

def main_menu():
    console = Console()

    panel_text = "\n".join(['[bold red]Bienvenido al OpenStack Manager[/bold red]', '1.Iniciar Sesion', '2. Salir'])
    panel = Panel(panel_text, title='Menu Principal')

    console.print(panel)

def main():
    while True:
        main_menu()
        choice = input("Seleccione una opción: ")

        if choice == "1":
            connection = login()
            if connection is not None:
                openstack_operations(connection)  # Llamar a la función correspondiente en openstack_menu.py

        elif choice == "2":
            print("Hasta luego.")
            break

if __name__ == "__main__":
    main()