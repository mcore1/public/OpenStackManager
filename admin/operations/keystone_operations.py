import sys
import os
from getpass import getpass
import openstack
#from ascii_art import ascii_art
from rich.console import Console

console = Console()
console.print("[bold yellow]OpenStack Keystone Operations[/bold yellow]")

def get_token(connection):
    # Obtener el token
    print("Token:", connection.session.get_token())

def create_user(connection):
    # Solicitar nombre de usuario
    username = input('Ingrese el nombre de usuario: ')

    # Solicitar contraseña
    password = getpass('Ingrese la contraseña: ')

    # Crear el usuario utilizando la conexión a Keystone
    user = connection.identity.create_user(name=username, password=password)
    print('Usuario creado con ID:', user.id)

def delete_user(connection, username):
    # Obtener la lista de usuarios
    users = connection.identity.users()

    # Buscar el usuario por nombre
    user_to_delete = None
    for user in users:
        if user.name == username:
            user_to_delete = user
            break

    if user_to_delete:
        # Eliminar el usuario utilizando la conexión a Keystone
        connection.identity.delete_user(user_to_delete.id)
        print(f"Usuario '{user_to_delete.name}' eliminado con éxito.")
    else:
        print(f"No se encontró el usuario '{username}'.")

def list_users(connection):
    # Obtener la lista de usuarios
    users = connection.identity.users()

    # Imprimir la lista de usuarios
    print("Lista de usuarios:")
    for user in users:
        print(f"ID: {user.id}, Nombre: {user.name}")
