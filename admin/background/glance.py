import sys
import os
from getpass import getpass

# Agregar la ruta del directorio raíz del proyecto al sys.path
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(root_dir)

# Importar el módulo login desde modules
from modules.login import login

# Obtener la conexión utilizando la función de login
connection = login()

# Obtener información del servicio Glance
glance = connection.image

def listar_imagenes():
    print("Listado de imágenes en Glance:")
    images = glance.images()
    for image in images:
        print("ID:", image.id)
        print("Name:", image.name)
        print("Status:", image.status)
        print("Size:", image.size)
        print("------------------------")

# Ejecutar la función para listar las imágenes
listar_imagenes()