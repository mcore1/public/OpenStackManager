import sys
import os
from getpass import getpass

# Agregar la ruta del directorio raíz del proyecto al sys.path
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(root_dir)

# Importar el módulo login desde modules
from modules.login import login

# Obtener la conexión utilizando la función de login
connection = login()

# # Obtener información del servicio Nova
# nova = connection.compute

# def listar_vms():
#     print("Listado de instancias en Nova:")
#     vms = nova.servers()
#     for vm in vms:
#         print("ID:", vm.id)
#         print("Name:", vm.name)
#         print("Status:", vm.status)
#         print("Flavor:", vm.flavor['id'])
#         print("------------------------")

# # Ejecutar la función para listar las instancias
# listar_vms()
# Obtener información del servicio Nova
nova = connection.compute

def listar_vms():
    print("Listado de instancias en Nova:")
    vms = nova.servers()
    for vm in vms:
        print("ID:", vm.id)
        print("Name:", vm.name)
        print("Status:", vm.status)
        print("Flavor:", vm.flavor['id'])
        print("------------------------")

def borrar_instancia():
    listar_vms()
    vm_id = input("Ingrese el ID de la instancia que desea borrar: ")

    try:
        nova.delete_server(vm_id)
        print("Instancia eliminada exitosamente.")
    except Exception as e:
        print("Error al eliminar la instancia:", str(e))

# Ejecutar la función para borrar una instancia
borrar_instancia()