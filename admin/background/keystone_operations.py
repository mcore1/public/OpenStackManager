import sys
import os
from getpass import getpass

# Agregar la ruta del directorio raíz del proyecto al sys.path
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(root_dir)

# Importar el módulo login desde modules
from modules.login import login

# Obtener la conexión utilizando la función de login
connection = login()

# Obtener información del servicio Keystone
keystone = connection.identity

def get_token():
    # Obtener el token
    print("Token:", connection.session.get_token())

def create_user():
    # Solicitar nombre de usuario
    username = input('Ingrese el nombre de usuario: ')

    # Solicitar contraseña
    password = getpass('Ingrese la contraseña: ')

    # Crear el usuario utilizando la conexión a Keystone
    user = connection.identity.create_user(name=username, password=password)
    print('Usuario creado con ID:', user.id)