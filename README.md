# OpenStackManager



## Getting started
Modulos y funciones

ADMIN
- Keystone
  - Get Token > OK (REDESIGN)
  - Crear Usuario > OK (redesign)
  - List Users > OK
  - Eliminar Usuario > OK
  - Crear Proyecto > OK
  - Dar Permisos
  - Eliminar Proyecto
  - Modificar Permisos 
- Glance
  - Subir imagen
  - Borrar Imagen
  - Listar Imágenes > OK (redesign)
  - Actualizar Información de una Imagen
- Nova
  - Crear Flavor
  - Listar Instancias
  - Reiniciar Instancia
  - Detener/Iniciar Instancia
- Neutron
  - Listar Redes
  - Eliminar Red
- Cinder
  - Listar Volumenes
  - Eliminar Volumenes


USUARIO FINAL
- Keystone
  - Cambiar password
- Glance
  - Listar Imagenes Disponibles
  - Descargar Imagen
- Nova
  - Crear server
  - Borrar Server
  - Resize server
  - Resize Vol
  - Listar mis instancias
  - Detalles de una instancia
- Neutron
  - Crear red tenant
  - Crear router
  - Asignar / Desasignar IP flotante
  - Cambiar reglas sec group
  - Listar mis redes
- Cinder
  - Crear / Borrar Vol.
  - Atatchar / detachar VOL.
  - Listar VOL
  - Crear snapshot de un volumen